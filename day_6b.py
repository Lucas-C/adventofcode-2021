#!/usr/bin/env python3

with open('input_6.txt') as in_file:
    numbers = list(map(int, in_file.readline().strip().split(',')))
# Converting to a more efficient datastructure:
fishes = [0]*9
for n in numbers:
    fishes[n] += 1
for _ in range(256):
    new_fishes = [0]*9
    for fish, count in enumerate(fishes):
        if fish == 0:
            new_fishes[6] += count
            new_fishes[8] += count
        else:
            new_fishes[fish-1] += count
    fishes = new_fishes
print(sum(fishes))