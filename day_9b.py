#!/usr/bin/env python3

with open('input_9.txt') as in_file:
    heightmap = [list(map(int, line.strip())) for line in in_file]
j_max = len(heightmap) - 1
i_max = len(heightmap[0]) - 1
low_points = []
for j, row in enumerate(heightmap):
    for i, height in enumerate(row):
        top = 10 if j == 0 else heightmap[j-1][i]
        bottom = 10 if j == j_max else heightmap[j+1][i]
        left = 10 if i == 0 else heightmap[j][i-1]
        right = 10 if i == i_max else heightmap[j][i+1]
        if top > height and bottom > height and left > height and right > height:
            low_points.append((i, j))
basin_sizes = []
for x, y in low_points:
    area = {(x, y)}
    while True:
        new_area = set(area)
        for i, j in area:
            if j > 0 and heightmap[j-1][i] < 9:
                new_area.add((i, j-1))
            if j < j_max and heightmap[j+1][i] < 9:
                new_area.add((i, j+1))
            if i > 0 and heightmap[j][i-1] < 9:
                new_area.add((i-1, j))
            if i < i_max and heightmap[j][i+1] < 9:
                new_area.add((i+1, j))
        if len(new_area) == len(area):
            break
        area = new_area
    basin_sizes.append(len(area))
a, b, c = sorted(basin_sizes)[-3:]
print(a*b*c)