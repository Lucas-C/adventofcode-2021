#!/usr/bin/env python3
from binascii import unhexlify


class Packet:
    def __init__(self, version, type):
        self.version = version
        self.type = type
    @staticmethod
    def parse(data):
        version, type, data = bitstr2dec(data[:3]), bitstr2dec(data[3:6]), data[6:]
        if type == 4:
            value, data = Literal.parse_value(data)
            return Literal(version, type, value), data
        subpackets, data = Operator.parse_subpackets(data)
        return Operator(version, type, subpackets), data
    def sum_versions(self):
        return self.version

class Literal(Packet):
    def __init__(self, version, type, value):
        super().__init__(version, type)
        self.value = value
    def __str__(self):
        return f'Literal({self.value})'
    @staticmethod
    def parse_value(data):
        bitstr = ''
        while True:
            group, data = data[:5], data[5:]
            bitstr += group[1:]
            if group[0] == '0':
                break
        return bitstr2dec(bitstr), data

class Operator(Packet):
    def __init__(self, version, type, subpackets):
        super().__init__(version, type)
        self.subpackets = subpackets
    def __str__(self):
        return f'Operator[{self.type}]({", ".join(map(str, self.subpackets))})'
    @staticmethod
    def parse_subpackets(data):
        subpackets = []
        subpackets_length, subpackets_count = None, None
        type_id, data = data[0], data[1:]
        if type_id == '0':
            subpackets_length = bitstr2dec(data[:15])
            data = data[15:]
        else:
            subpackets_count = bitstr2dec(data[:11])
            data = data[11:]
        count, initial_data_len = 0, len(data)
        while True:
            packet, data = Packet.parse(data)
            if packet:
                subpackets.append(packet)
                count += 1
                if count == subpackets_count or (initial_data_len - len(data)) == subpackets_length:
                    break
            else:
                break
        return subpackets, data
    def sum_versions(self):
        return self.version + sum(p.sum_versions() for p in self.subpackets)

def hex2bitstr(hex):
    bitstr = ''
    for byte in unhexlify(hex):
        bitstr += '1' if (byte & 128) else '0'
        bitstr += '1' if (byte & 64) else '0'
        bitstr += '1' if (byte & 32) else '0'
        bitstr += '1' if (byte & 16) else '0'
        bitstr += '1' if (byte & 8) else '0'
        bitstr += '1' if (byte & 4) else '0'
        bitstr += '1' if (byte & 2) else '0'
        bitstr += '1' if (byte & 1) else '0'
    return bitstr

def test_hex2bitstr():
    assert hex2bitstr('D2FE28') == '110100101111111000101000'
    assert hex2bitstr('38006F45291200') == '00111000000000000110111101000101001010010001001000000000'
    assert hex2bitstr('EE00D40C823060') == '11101110000000001101010000001100100000100011000001100000'

def bitstr2dec(bits):
    n, bits = 0, list(bits)
    while bits:
        n <<= 1
        if bits.pop(0) == '1':
            n += 1
    return n

def test_bitstr2dec():
    assert bitstr2dec('011111100101') == 2021


if __name__ == '__main__':
    with open('input_16.txt') as in_file:
        hex_data = in_file.read().strip()
    data = hex2bitstr(hex_data)
    top_packet, remaining_data = Packet.parse(data)
    # print('top_packet:', top_packet)
    # print('remaining_data:', remaining_data)
    print(top_packet.sum_versions())
