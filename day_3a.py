#!/usr/bin/env python3

with open('input_3.txt') as in_file:
    diagnostic = [line.strip() for line in in_file]
bits_count = len(diagnostic[0])
gamma, epsilon = 0, 0
diag_freq = [0]*bits_count
for number in diagnostic:
    for i, digit in enumerate(number):
        if digit == '1':
            diag_freq[i] += 1
        else:
            diag_freq[i] -= 1
for freq in diag_freq:
    gamma <<= 1
    epsilon <<= 1
    if freq > 0:
        gamma += 1
    if freq < 0:
        epsilon += 1
print(gamma*epsilon)
