#!/usr/bin/env python3
from math import copysign


x_min, x_max, y_min, y_max = 20, 30, -10, -5  # base example values

def fire(vx, vy):
    x, y, peak = 0, 0, 0
    while True:
        x += vx
        if vx:
            vx -= copysign(1, vx)
        y += vy
        vy -= 1
        if y > peak:
            peak = y
        if x_min <= x <= x_max and y_min <= y <= y_max:
            return peak
        if x > x_max or y < y_min:
            return None

def test_fire():
    assert fire(7, 2) == 3
    assert fire(6, 3) == 6
    assert fire(9, 0) == 0
    assert fire(17, -4) == None

if __name__ == '__main__':
    with open('input_17.txt') as in_file:
        line = in_file.read().strip()
    x, y = line[13:].split(', ')
    x_min, x_max = x.split('=')[1].split('..')
    x_min, x_max = int(x_min), int(x_max)
    y_min, y_max = y.split('=')[1].split('..')
    y_min, y_max = int(y_min), int(y_max)

    max_peak = 0
    for vx in range(1, x_max+1):
        for vy in range(1, max(abs(y_max), abs(y_min))):
            peak = fire(vx, vy)
            if peak and peak > max_peak:
                max_peak = peak
    print(max_peak)
