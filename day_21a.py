#!/usr/bin/env python3

with open('input_21.txt') as in_file:
    pos1 = int(in_file.readline().strip()[-1])
    pos2 = int(in_file.readline().strip()[-1])

# pos1, pos2 = 4, 8  # example data
score1, score2 = 0, 0

die = 100
def roll():
    global die
    die += 1
    die %= 100
    return die

for turn in range(1, 1000):
    pos1 += roll() + roll() + roll()
    pos1 = (pos1 - 1)%10 + 1
    score1 += pos1
    if score1 >= 1000:
        print(score2*(turn*6-3))
        break
    pos2 += roll() + roll() + roll()
    pos2 = (pos2 - 1)%10 + 1
    score2 += pos2
    if score2 >= 1000:
        print(score1*turn*6)
        break
