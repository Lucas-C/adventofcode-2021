#!/usr/bin/env python3
from collections import defaultdict
from typing import NamedTuple


class Cuboid(NamedTuple):
    state: bool
    x_min: int
    x_max: int
    y_min: int
    y_max: int
    z_min: int
    z_max: int


cuboids = []
with open('input_22.txt') as in_file:
    for line in in_file:
        state, xyz = line.rstrip().split(' ')
        x, y, z = xyz.split(',')
        x_min, x_max = x.split('=')[1].split('..')
        y_min, y_max = y.split('=')[1].split('..')
        z_min, z_max = z.split('=')[1].split('..')
        cuboids.append(Cuboid(state == 'on',
                              int(x_min), int(x_max),
                              int(y_min), int(y_max),
                              int(z_min), int(z_max)))

count = len(cuboids)
cuboids = [cuboid for cuboid in cuboids if (cuboid.x_min > -50 and cuboid.x_max < 50
                                        and cuboid.y_min > -50 and cuboid.y_max < 50
                                        and cuboid.z_min > -50 and cuboid.z_max < 50)]
print(f"{count - len(cuboids)} cuboids have been excluded because ENTIRELY outside the -50/50 coords region")
count = len(cuboids)
cuboids = [cuboid for cuboid in cuboids if (cuboid.x_min > -50 or cuboid.x_max < 50
                                         or cuboid.y_min > -50 or cuboid.y_max < 50
                                         or cuboid.z_min > -50 or cuboid.z_max < 50)]
print(f"{count - len(cuboids)} cuboids have been excluded because PARTIALLY outside the -50/50 coords region")


reactor = defaultdict(lambda: defaultdict(lambda: defaultdict(bool)))

for cuboid in cuboids:
    for x in range(cuboid.x_min, cuboid.x_max + 1):
        for y in range(cuboid.y_min, cuboid.y_max + 1):
            for z in range(cuboid.z_min, cuboid.z_max + 1):
                reactor[x][y][z] = cuboid.state

count = 0
for x in range(-50, 51):
    for y in range(-50, 51):
        for z in range(-50, 51):
            if reactor[x][y][z]:
                count += 1

print(count)
