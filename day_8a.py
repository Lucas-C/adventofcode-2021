#!/usr/bin/env python3

with open('input_8.txt') as in_file:
    lines = in_file.readlines()

count = 0
for line in lines:
    line = line.strip().split(' ')
    assert line[10] == '|' and len(line[11:]) == 4
    count += sum(1 for value in line[11:] if len(value) in (2, 3, 4, 7))
print(count)
