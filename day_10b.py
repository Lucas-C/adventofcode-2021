#!/usr/bin/env python3

OPENING_CHARS_OPPOSED = {'(': ')', '[': ']', '{': '}', '<': '>'}
CLOSING_CHARS_SCORES = {')': 1, ']': 2, '}': 3, '>': 4}

with open('input_10.txt') as in_file:
    chunks = [line.strip() for line in in_file]

scores = []
for chunk in chunks:
    stack = []
    for c in chunk:
        if c in OPENING_CHARS_OPPOSED:
            stack.append(c)
        else:
            if c == OPENING_CHARS_OPPOSED[stack[-1]]:
                stack.pop()
            else:
                break
    else:
        score = 0
        for c in reversed(stack):
            score *= 5
            score += CLOSING_CHARS_SCORES[OPENING_CHARS_OPPOSED[c]]
        scores.append(score)
scores = sorted(scores)
print(scores[len(scores)//2])
    