#!/usr/bin/env python3

with open('input_1.txt') as in_file:
    measurements = [int(line.strip()) for line in in_file]
largers, last_sum = 0, None
for i, measurement in enumerate(measurements):
    if i >= 2:
        sum3 = measurements[i-2] + measurements[i-1] + measurement
        if last_sum is not None and sum3 > last_sum:
            largers += 1
        last_sum = sum3
print(largers)
