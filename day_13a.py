#!/usr/bin/env python3

dots, folds = set(), []
dots_reading = True  # False => now reading folds
with open('input_13.txt') as in_file:
    for line in in_file:
        line = line.strip()
        if not line:
            dots_reading = False
        elif dots_reading:
            x, y = line.split(',')
            x, y = int(x), int(y)
            dots.add((x, y))
        else:
            assert line.startswith('fold along ')
            line = line[len('fold along '):]
            axis, value = line.split('=')
            folds.append((axis, int(value)))

for axis, value in folds:
    new_dots = set()
    for x, y in dots:
        if axis == 'x':
            if x > value:
                x = 2*value - x
        else:  # axis == 'y'
            if y > value:
                y = 2*value - y
        new_dots.add((x, y))
    dots = new_dots
    break

print(len(dots))
