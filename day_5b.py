#!/usr/bin/env python3

vents = []
max_x, max_y = 0, 0
with open('input_5.txt') as in_file:
    while True:
        line = in_file.readline()
        if not line:
            break
        x1y1, x2y2 = line.split(' -> ')
        x1, y1 = map(int, x1y1.split(','))
        if x1 > max_x: max_x = x1
        if y1 > max_y: max_y = y1
        x2, y2 = map(int, x2y2.split(','))
        if x2 > max_x: max_x = x2
        if y2 > max_y: max_y = y2
        vents.append(((x1, y1), (x2, y2)))
diagram = []
for _ in range(max_y+1):
    diagram.append([0]*(max_x+1))
for (x1, y1), (x2, y2) in vents:
    assert x1 == x2 or y1 == y2 or abs(x1-x2) == abs(y1-y2)
    delta_x = 0
    if x2 > x1: delta_x = +1
    if x2 < x1: delta_x = -1
    delta_y = 0
    if y2 > y1: delta_y = +1
    if y2 < y1: delta_y = -1
    while x1 != x2 or y1 != y2:
        diagram[y1][x1] += 1
        x1 += delta_x
        y1 += delta_y
    diagram[y1][x1] += 1
print(sum(sum(1 for n in line if n > 1) for line in diagram))
