#!/usr/bin/env python3
from collections import defaultdict

with open('input_12.txt') as in_file:
    paths = [line.strip() for line in in_file]

graph = defaultdict(list)  # node -> [node]
for path in paths:
    node1, node2 = path.split('-')
    graph[node1].append(node2)
    graph[node2].append(node1)

def iter_paths(path, twice=False):
    for node in graph[path[-1]]:
        is_small_cave = node.lower() == node
        if node == 'start':
            continue
        elif node == 'end':
            yield path + ['end']
        elif not is_small_cave or node not in path:
            yield from iter_paths(path + [node], twice=twice)
        elif node in path and is_small_cave and not twice:
            yield from iter_paths(path + [node], twice=True)

print(len(list(iter_paths(['start']))))
