#!/usr/bin/env python3

from day_18a import reduce, magnitude


if __name__ == '__main__':
    MAX_MAG = 0
    VALUES = []
    with open('input_18.txt') as in_file:
        for line in in_file:
            NEW_VAL = eval(line)
            for VAL in VALUES:
                MAG = magnitude(reduce([VAL, NEW_VAL]))
                if MAG > MAX_MAG: MAX_MAG = MAG
                MAG = magnitude(reduce([NEW_VAL, VAL]))
                if MAG > MAX_MAG: MAX_MAG = MAG
            VALUES.append(NEW_VAL)
    print(MAX_MAG)
