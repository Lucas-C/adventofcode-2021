#!/usr/bin/env python3


def depth(pair):
    if isinstance(pair, int):
        return 0
    assert isinstance(pair, list)
    return 1 + max(depth(pair[0]), depth(pair[1]))

def test_depth():
    assert depth(1) == 0
    assert depth([1, 2]) == 1
    assert depth([[1, 2], 3]) == 2

def digit_max(pair):
    if isinstance(pair, int):
        return pair
    assert isinstance(pair, list)
    return max(digit_max(pair[0]), digit_max(pair[1]))

def test_digit_max():
    assert digit_max(1) == 1
    assert digit_max([1, 2]) == 2
    assert digit_max([[1, 2], 3]) == 3

def insert_left(val, pair):
    if isinstance(pair, int):
        return val + pair
    new_pair0 = insert_left(val, pair[0])
    if new_pair0 is False:
        return False
    return [new_pair0, pair[1]]

def test_insert_left():
    assert insert_left(1, 2) == 3
    assert insert_left(1, [2, 3]) == [3, 3]
    assert insert_left(1, [[2, 3], 4]) == [[3, 3], 4]

def insert_right(pair, val):
    if isinstance(pair, int):
        return pair + val
    new_pair1 = insert_right(pair[1], val)
    if new_pair1 is False:
        return False
    return [pair[0], new_pair1]

def explode(pair):
    '''
    Result:
        * False if pair does not need explosion
        * new_pair otherwise
    '''
    result = _explode(pair, depth=0)
    if result is False:
        return False
    return result[1]

def _explode(pair, depth):
    '''
    Result:
        * False if pair does not need explosion
        * (left, new_pair, right) otherwise
    '''
    if isinstance(pair, int):
        return False
    if depth == 4:
        return pair[0], 0, pair[1]
    result = _explode(pair[0], depth+1)
    if result:
        left, subpair, right = result
        pair = [subpair, pair[1]]
        if right is not None:
            new_pair1 = insert_left(right, pair[1])
            if new_pair1:
                pair, right = [subpair, new_pair1], None
        return left, pair, right
    result = _explode(pair[1], depth+1)
    if result:
        left, subpair, right = result
        pair = [pair[0], subpair]
        if left is not None:
            new_pair0 = insert_right(pair[0], left)
            if new_pair0:
                left, pair = None, [new_pair0, subpair]
        return left, pair, right
    return False

def test_explode():
    assert explode(1) == False
    assert explode([1, 2]) == False
    assert explode([[1, 2], 3]) == False
    assert explode([[[1, 2], 3], 4]) == False
    assert explode([[[[1, 2], 3], 4], 5]) == False
    assert explode([[[[[9,8],1],2],3],4]) == [[[[0,9],2],3],4]
    assert explode([7,[6,[5,[4,[3,2]]]]]) == [7,[6,[5,[7,0]]]]
    assert explode([[6,[5,[4,[3,2]]]],1]) == [[6,[5,[7,0]]],3]
    assert explode([[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]) == [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]
    assert explode([[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]) == [[3,[2,[8,0]]],[9,[5,[7,0]]]]

def split(pair):
    '''
    Result:
        * False if pair does not need splitting
        * new_pair otherwise
    '''
    if isinstance(pair, int):
        if pair >= 10:
            return [pair//2, pair//2 + (pair%2)]
        return False
    new_pair0 = split(pair[0])
    if new_pair0:
        return [new_pair0, pair[1]]
    new_pair1 = split(pair[1])
    if new_pair1:
        return [pair[0], new_pair1]
    return False

def test_split():
    assert split(1) == False
    assert split([1, 2]) == False
    assert split(10) == [5, 5]
    assert split(11) == [5, 6]
    assert split([12, 3]) == [[6, 6], 3]

def reduce(pair):
    result = explode(pair)
    if result:
        return reduce(result)
    result = split(pair)
    if result:
        return reduce(result)
    return pair

def test_reduce():
    assert reduce([[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]) == [[[[0,7],4],[[7,8],[6,0]]],[8,1]]

def magnitude(pair):
    if isinstance(pair, int):
        return pair
    return 3*magnitude(pair[0]) + 2*magnitude(pair[1])

if __name__ == '__main__':
    VAL = None
    with open('input_18.txt') as in_file:
        for line in in_file:
            NEW_VAL = eval(line)
            VAL = NEW_VAL if VAL is None else reduce([VAL, NEW_VAL])
    print(magnitude(VAL))
