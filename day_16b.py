#!/usr/bin/env python3
from day_16a import hex2bitstr, bitstr2dec


class Packet:
    def __init__(self, version, type):
        self.version = version
        self.type = type
    @staticmethod
    def parse(data):
        version, type, data = bitstr2dec(data[:3]), bitstr2dec(data[3:6]), data[6:]
        if type == 4:
            value, data = Literal.parse_value(data)
            return Literal(version, type, value), data
        subpackets, data = Operator.parse_subpackets(data)
        return Operator(version, type, subpackets), data
    def compute(self):
        raise NotImplementedError

class Literal(Packet):
    def __init__(self, version, type, value):
        super().__init__(version, type)
        self.value = value
    def __str__(self):
        return f'Literal({self.value})'
    @staticmethod
    def parse_value(data):
        bitstr = ''
        while True:
            group, data = data[:5], data[5:]
            bitstr += group[1:]
            if group[0] == '0':
                break
        return bitstr2dec(bitstr), data
    def compute(self):
        return self.value

class Operator(Packet):
    def __init__(self, version, type, subpackets):
        super().__init__(version, type)
        self.subpackets = subpackets
    def __str__(self):
        return f'Operator[{self.type}]({", ".join(map(str, self.subpackets))})'
    @staticmethod
    def parse_subpackets(data):
        subpackets = []
        subpackets_length, subpackets_count = None, None
        type_id, data = data[0], data[1:]
        if type_id == '0':
            subpackets_length = bitstr2dec(data[:15])
            data = data[15:]
        else:
            subpackets_count = bitstr2dec(data[:11])
            data = data[11:]
        count, initial_data_len = 0, len(data)
        while True:
            packet, data = Packet.parse(data)
            if packet:
                subpackets.append(packet)
                count += 1
                if count == subpackets_count or (initial_data_len - len(data)) == subpackets_length:
                    break
            else:
                break
        return subpackets, data
    def compute(self):
        if self.type == 0:
            return sum(p.compute() for p in self.subpackets)
        elif self.type == 1:
            result = 1
            for p in self.subpackets:
                result *= p.compute()
            return result
        elif self.type == 2:
            assert self.subpackets
            return min(p.compute() for p in self.subpackets)
        elif self.type == 3:
            assert self.subpackets
            return max(p.compute() for p in self.subpackets)
        elif self.type == 5:
            assert len(self.subpackets) == 2
            return 1 if self.subpackets[0].compute() > self.subpackets[1].compute() else 0
        elif self.type == 6:
            assert len(self.subpackets) == 2
            return 1 if self.subpackets[0].compute() < self.subpackets[1].compute() else 0
        elif self.type == 7:
            assert len(self.subpackets) == 2
            return 1 if self.subpackets[0].compute() == self.subpackets[1].compute() else 0
        assert False, self.type


if __name__ == '__main__':
    with open('input_16.txt') as in_file:
        hex_data = in_file.read().strip()
    data = hex2bitstr(hex_data)
    top_packet, remaining_data = Packet.parse(data)
    # print('top_packet:', top_packet)
    # print('remaining_data:', remaining_data)
    print(top_packet.compute())
