#!/usr/bin/env python3

# len(polymer) growth is around 19.2^n + 1

from collections import defaultdict

rules = {}
with open('input_14.txt') as in_file:
    polymer = in_file.readline().strip()
    in_file.readline()  # empty line
    for line in in_file:
        line = line.strip()
        from_, to_ = line.split(' -> ')
        rules[from_] = to_

# We convert the rules logic to a (pair) -> (pair) conversion.
# This way, we can only track a counter per pair, and still deduce the correct chain growth:
rules2 = {}
for pair, elem in rules.items():
    rules2[pair] = (pair[0]+elem, elem+pair[1])
start, end = polymer[0], polymer[-1]
stats = defaultdict(int)
for c1, c2 in zip(polymer, polymer[1:]):
    stats[c1+c2] += 1

for step in range(40):
    new_stats = defaultdict(int)
    for pair, count in stats.items():
        for next_pair in rules2[pair]:
            new_stats[next_pair] += count
    stats = new_stats

elem_stats = defaultdict(int)
for pair, count in stats.items():
    for c in pair:
        elem_stats[c] += count
# All elements except the 2 ones at the beginning & end of the chain are counted twice, in 2 overlapping pairs:
elem_stats[start] += 1
elem_stats[end] += 1
max_count, min_count = 0, float('Inf')
for elem, count in elem_stats.items():
    assert count % 2 == 0
    count //= 2
    if count > max_count: max_count = count
    if count < min_count: min_count = count
print(max_count - min_count)
