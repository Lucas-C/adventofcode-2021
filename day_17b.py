#!/usr/bin/env python3

from day_17a import fire


if __name__ == '__main__':
    with open('input_17.txt') as in_file:
       line = in_file.read().strip()
    x, y = line[13:].split(', ')
    x_min, x_max = x.split('=')[1].split('..')
    x_min, x_max = int(x_min), int(x_max)
    y_min, y_max = y.split('=')[1].split('..')
    y_min, y_max = int(y_min), int(y_max)

    velocities = set()
    for vx in range(1, x_max+1):
        for vy in range(y_min, max(abs(y_max), abs(y_min))):
            peak = fire(vx, vy)
            if peak is not None:
                velocities.add((vx, vy))
    print(len(velocities))
