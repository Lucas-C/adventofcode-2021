#!/usr/bin/env python3
from typing import NamedTuple


class Cuboid(NamedTuple):
    state: bool
    x_min: int
    x_max: int
    y_min: int
    y_max: int
    z_min: int
    z_max: int
    def count_on(self):
        count = (self.x_max - self.x_min + 1) * (self.y_max - self.y_min + 1) * (self.z_max - self.z_min + 1)
        if not self.state:
            return -count
        return count
    def is_overlapping(self, cuboid):
        # Formula from: https://developer.mozilla.org/en-US/docs/Games/Techniques/3D_collision_detection#aabb_vs._aabb
        return (self.x_min <= cuboid.x_max and self.x_max >= cuboid.x_min
            and self.y_min <= cuboid.y_max and self.y_max >= cuboid.y_min
            and self.z_min <= cuboid.z_max and self.z_max >= cuboid.z_min)
    def intersect(self, cuboid, state):
        'Returns a Cuboid in the given state, or None'
        if not self.is_overlapping(cuboid):
            return None
        return Cuboid(state,
                      max(self.x_min, cuboid.x_min), min(self.x_max, cuboid.x_max),
                      max(self.y_min, cuboid.y_min), min(self.y_max, cuboid.y_max),
                      max(self.z_min, cuboid.z_min), min(self.z_max, cuboid.z_max))


def cuboids_count_on(in_cuboids):
    reactor = []  # list of Cuboids
    for in_cuboid in in_cuboids:  # insertion order matters
        new_cuboids = []
        for cuboid in reactor:
            inbetween = cuboid.intersect(in_cuboid, state=not cuboid.state)
            if inbetween:
                new_cuboids.append(inbetween)
        if in_cuboid.state:
            new_cuboids.append(in_cuboid)
        reactor.extend(new_cuboids)

    count = 0
    for c in reactor:
        count += c.count_on()
    return count

def test_cuboids_count_on_1dimension():
    def line(start, end, state=True):
        return Cuboid(state, start, end, y_min=0, y_max=0, z_min=0, z_max=0)

    A = line(1, 10)
    B = line(6, 15)
    assert cuboids_count_on((A, B)) == 15
    C = line(4, 12)
    assert cuboids_count_on((A, B, C))  == 15
    D = line(3, 16)
    assert cuboids_count_on((A, B, C, D)) == 16

    C = line(4, 12, state=False)
    assert cuboids_count_on((A, B, C))  == 15 - abs(C.count_on())
    D = line(3, 16, state=False)
    assert cuboids_count_on((A, B, C, D)) == 2

def test_cuboids_count_on_2dimensions():
    def square(x_range, y_range, state=True):
        return Cuboid(state, *x_range, *y_range, z_min=0, z_max=0)

    A = square((1, 4), (1, 4))
    B = square((4, 5), (4, 5))
    assert cuboids_count_on((A, B)) == 19  # 16 + 3

    A = square((1, 4), (1, 4))
    B = square((3, 5), (1, 5))
    assert cuboids_count_on((A, B)) == 23  # 16 + 7
    C = square((2, 7), (3, 6))
    assert cuboids_count_on((A, B, C)) == 36  # 23 + 13
    D = square((4, 6), (2, 7))
    assert cuboids_count_on((A, B, C, D)) == 40  # 36 + 4
    E = square((1, 2), (1, 2), state=False)
    assert cuboids_count_on((A, B, C, D, E)) == 36  # 40 - 2x2
    F = square((1, 4), (1, 2), state=False)
    assert cuboids_count_on((A, B, C, D, E, F)) == 32  # 36 - 2x2


if __name__ == '__main__':
    in_cuboids = []
    with open('input_22.txt') as in_file:
        for line in in_file:
            state, xyz = line.rstrip().split(' ')
            x, y, z = xyz.split(',')
            x_min, x_max = x.split('=')[1].split('..')
            y_min, y_max = y.split('=')[1].split('..')
            z_min, z_max = z.split('=')[1].split('..')
            in_cuboids.append(Cuboid(state == 'on',
                                     int(x_min), int(x_max),
                                     int(y_min), int(y_max),
                                     int(z_min), int(z_max)))
    print(f"#in_cuboids={len(in_cuboids)}")
    print(cuboids_count_on(in_cuboids))
