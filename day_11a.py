#!/usr/bin/env python3

with open('input_11.txt') as in_file:
    grid = [list(map(int, line.strip())) for line in in_file]

j_max = len(grid) - 1
i_max = len(grid[0]) - 1

flashes_count = 0
for step in range(100):
    for row in grid:
        for i in range(len(row)):
            row[i] += 1
    flashed_pos = set()
    while True:
        new_flashed_pos = set(flashed_pos)
        for j, row in enumerate(grid):
            for i, energy in enumerate(row):
                if energy > 9 and (i, j) not in flashed_pos:
                    new_flashed_pos.add((i, j))
                    flashes_count += 1
                    # Orthogonal:
                    if i > 0: grid[j][i-1] += 1
                    if i < i_max: grid[j][i+1] += 1
                    if j > 0: grid[j-1][i] += 1
                    if j < j_max: grid[j+1][i] += 1
                    # Diagonal:
                    if i > 0 and j > 0: grid[j-1][i-1] += 1
                    if i > 0 and j < j_max: grid[j+1][i-1] += 1
                    if i < i_max and j > 0: grid[j-1][i+1] += 1
                    if i < i_max and j < j_max: grid[j+1][i+1] += 1
        if len(new_flashed_pos) == len(flashed_pos):
            break
        flashed_pos = new_flashed_pos
    for i, j in flashed_pos:
        grid[j][i] = 0
    # for row in grid:
        # print(''.join(map(str, row)))
    # print()
print('flashes_count:', flashes_count)
