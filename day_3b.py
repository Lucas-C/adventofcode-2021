#!/usr/bin/env python3

def binstr2in(binstr):
    result = 0
    for c in binstr:
        result <<= 1
        if c == '1':
            result += 1
    return result

with open('input_3.txt') as in_file:
    diagnostic = [line.strip() for line in in_file]
bits_count = len(diagnostic[0])
lines = diagnostic
for bit_i in range(bits_count):
    ones = sum(1 for line in lines if line[bit_i] == '1')
    zeros = len(lines) - ones
    if ones >= zeros:
        lines = [line for line in lines if line[bit_i] == '1']
    else:
        lines = [line for line in lines if line[bit_i] == '0']
    if len(lines) == 1:
        oxygen = binstr2in(lines[0])
        break
lines = diagnostic
for bit_i in range(bits_count):
    ones = sum(1 for line in lines if line[bit_i] == '1')
    zeros = len(lines) - ones
    if ones < zeros:
        lines = [line for line in lines if line[bit_i] == '1']
    else:
        lines = [line for line in lines if line[bit_i] == '0']
    if len(lines) == 1:
        co2 = binstr2in(lines[0])
        break
print(oxygen*co2)
