#!/usr/bin/env python3

with open('input_9.txt') as in_file:
    heightmap = [list(map(int, line.strip())) for line in in_file]
j_max = len(heightmap) - 1
i_max = len(heightmap[0]) - 1
total = 0
for j, row in enumerate(heightmap):
    for i, height in enumerate(row):
        top = 10 if j == 0 else heightmap[j-1][i]
        bottom = 10 if j == j_max else heightmap[j+1][i]
        left = 10 if i == 0 else heightmap[j][i-1]
        right = 10 if i == i_max else heightmap[j][i+1]
        if top > height and bottom > height and left > height and right > height:
            total += height + 1
print(total)