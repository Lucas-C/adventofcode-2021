#!/usr/bin/env python3
from dataclasses import dataclass, field
from queue import PriorityQueue
from typing import Any, NamedTuple, Tuple


ENERGY_COST = {
    'A': 1,
    'B': 10,
    'C': 100,
    'D': 1000,
}

class Move(NamedTuple):
    src_pos: Tuple[int]
    dst_pos: Tuple[int]
    energy: int

class Solution(NamedTuple):
    energy: int  # total energy from start to finish
    moves: Tuple[Move]

@dataclass(order=True)  # Recipe from: https://docs.python.org/3/library/queue.html#queue.PriorityQueue
class Node:  # Node for the A* algorithm = item in the PriorityQueue
    estimated_dist: int
    burrow: Any = field(compare=False)

class BurrowState:
    def __init__(self, lines):
        self.cave = lines
        self.dist = self.dist_to_solution()
    def __hash__(self):
        return hash(self.cave)
    def __str__(self):
        return ''.join(self.cave)
    def is_solved(self):
        l = self.cave
        return (l[2][3] == 'A' and l[3][3] == 'A'
            and l[2][5] == 'B' and l[3][5] == 'B'
            and l[2][7] == 'C' and l[3][7] == 'C'
            and l[2][9] == 'D' and l[3][9] == 'D')
    def apply(self, move):
        lines = []
        for line in self.cave:
            lines.append(list(line))
        src_i, src_j = move.src_pos
        dst_i, dst_j = move.dst_pos
        lines[dst_j][dst_i] = lines[src_j][src_i]
        lines[src_j][src_i] = '.'
        return BurrowState(tuple(''.join(line) for line in lines))
    def available_moves(self, prev_moves):
        for i, j, amphi in self.iter_amphis():
            for x, y in self.empty_neighbours(i, j):
                if self.is_ok(amphi, (i, j), (x, y), prev_moves):
                    yield Move((i, j), (x, y), ENERGY_COST[amphi])
    def iter_amphis(self):
        for j in range(1, 4):
            for i in range(1, 12):
                cell = self.cave[j][i]
                if cell in 'ABCD':
                    yield i, j, cell
    def empty_neighbours(self, i, j):
        if self.cave[j][i+1] == '.': yield i+1, j
        if self.cave[j][i-1] == '.': yield i-1, j
        if self.cave[j+1][i] == '.': yield i, j+1
        if self.cave[j-1][i] == '.': yield i, j-1
    def is_ok(self, amphi, src_pos, dst_pos, prev_moves):
        'Check extra rules'
        last_move = prev_moves[-1] if prev_moves else None
        amphi_moved_last = last_move and src_pos == last_move.dst_pos
        if last_move and not amphi_moved_last:
            x, y = last_move.dst_pos
            if y == 1:
                if x in (3, 5, 7, 9):  # RULE 1: amphi cannot stop on the space immediately outside a room
                    return False
                # RULE 3: amphi cannot "pause & resume" its move in the hallway without leaving it
                index = len(prev_moves) - 2
                while index >= 0 and last_move.src_pos[1] == 1 and prev_moves[index].dst_pos == last_move.src_pos:
                    last_move = prev_moves[index]
                    index -= 1
                if index >= 0 and last_move.src_pos[1] == 1:
                    # => amphi made a move that started in the hallway and finished in the hallway
                    return False
        i, j = src_pos
        x, y = dst_pos
        if y == 2:
            assert i == x
            dst_room = 'ABCD'.index(amphi)*2 + 3
            if j == 1:  # RULE 2: entering a room from the hallway
                if self.cave[3][i] not in ('.', amphi):
                    return False
                if dst_room != i:
                    return False
            if j == 3:  # custom rule: amphi cannot leave the bottom of its dst room
                if dst_room == i:
                    return False
        return True
    def dist_to_solution(self):  # heuristic estimating the proximity to the final burrow state
        'Compute the theoretical minimal energy required to move all amphis to their room'
        dist = 0
        for i, j, amphi in self.iter_amphis():
            dst_room = 'ABCD'.index(amphi)*2 + 3
            if i == dst_room:
                dist += ENERGY_COST[amphi] if j == 1 else 0
            else:
                dist += (1 + abs(dst_room - i)) * ENERGY_COST[amphi]
                if j > 1:
                    dist += ENERGY_COST[amphi]
        return dist

def a_star_solve(start):
    'Reference: https://en.wikipedia.org/wiki/A*_search_algorithm#Pseudocode'
    INFINITE = float('inf')
    open_list = PriorityQueue()  # the nodes to explore (BurrowStates), with their cost (total energy)
    open_list.put(Node(0, start))
    dist_estim = {hash(start): start.dist}  # fScore[n] is our current best guess as to how short a path from start to finish can be if it goes through n
    energy_from_start = {hash(start): 0}  # gScore[n] is the cost of the cheapest path from start to n currently known
    leading_moves = {hash(start): ()}  # cameFrom / closed list / nodes already visited: leading_moves[n] is the list of moves preceding the node on the cheapest path from start
    while not open_list.empty():
        burrow = open_list.get().burrow  # we do not care about the Node priorization / heuristic value
        h = hash(burrow)
        current_energy = energy_from_start[h]
        prev_moves = leading_moves[h]
        if burrow.is_solved():
            return Solution(current_energy, prev_moves)
        for move in burrow.available_moves(prev_moves):
            neighbour = burrow.apply(move)
            nh = hash(neighbour)
            neighbour_energy = current_energy + move.energy  # "tentative_gScore"
            if neighbour_energy < energy_from_start.get(nh, INFINITE):
                energy_from_start[nh] = neighbour_energy
                estimated_dist = neighbour_energy + neighbour.dist
                if estimated_dist < dist_estim.get(nh, INFINITE):
                    dist_estim[nh] = estimated_dist
                    leading_moves[nh] = prev_moves + (move,)
                    open_list.put(Node(estimated_dist, neighbour))
    assert False  # => means A* found no solution


def test_BurrowState_hashability():
    with open('input_23.txt') as in_file:
        burrow1 = BurrowState(tuple(line for line in in_file))
    with open('input_23.txt') as in_file:
        burrow2 = BurrowState(tuple(line for line in in_file))
    assert hash(burrow1) == hash(burrow2)
    s = set()
    s.add(hash(burrow1))
    s.add(hash(burrow2))
    assert len(s) == 1

    burrow1 = BurrowState((
        '#############\n',
        '#..A........#\n',
        '###.#B#C#D###\n',
        '  #A#B#C#D#  \n',
        '  #########  \n',
    ))
    burrow2 = BurrowState((
        '#############\n',
        '#........D..#\n',
        '###A#B#C#.###\n',
        '  #A#B#C#D#  \n',
        '  #########  \n',
    ))
    assert hash(burrow1) != hash(burrow2)
    s = set()
    s.add(hash(burrow1))
    s.add(hash(burrow2))
    assert len(s) == 2

def test_BurrowState_dist_to_solution():
    with open('input_23.txt') as in_file:
        burrow = BurrowState(tuple(line for line in in_file))
    assert burrow.dist_to_solution() == 8*1000 + (6+4)*100 + (6+4)*10 + (6+4)*1
    burrow = BurrowState((
        '#############\n',
        '#...........#\n',
        '###A#B#C#D###\n',
        '  #A#B#C#D#  \n',
        '  #########  \n',
    ))
    assert burrow.dist_to_solution() == 0


def test_BurrowState_solve():
    burrow = BurrowState((
        '#############\n',
        '#...........#\n',
        '###B#A#C#D###\n',
        '  #A#B#C#D#  \n',
        '  #########  \n',
    ))
    assert a_star_solve(burrow).energy == 46


if __name__ == '__main__':
    with open('input_23.txt') as in_file:
        burrow = BurrowState(tuple(line for line in in_file))
    best_solution = a_star_solve(burrow)
    print(best_solution.energy)  # This currently works with the example provided but not the puzzle input :(
    #for move in best_solution.moves:
    #    burrow = burrow.apply(move)
    #    print(burrow)
