#!/usr/bin/env python3

with open('input_20.txt') as in_file:
    algo = in_file.readline().strip()
    in_file.readline()
    grid = [line.strip() for line in in_file]

assert len(grid) == len(grid[0])

def grow_grid(grid):
    n = len(grid)
    new_grid = []
    for i in range(n + 4):
        if i in (0, 1, n+2, n+3):
            new_grid.append(['.']*(n + 4))
        else:
            new_grid.append(['.', '.']+list(grid[i-2])+['.', '.'])
    return new_grid

def pos2index(grid, i, j, default):
    n = len(grid)
    bits = ''
    for x, y in ((i-1, j-1), (i, j-1), (i+1, j-1),
                 (i-1, j),   (i, j),   (i+1, j),
                 (i-1, j+1), (i, j+1), (i+1, j+1)):
        if x < 0 or y < 0 or x >= n or y >= n:
            cell = default
        else:
            cell = grid[y][x]
        bits += '1' if cell == '#' else '0'
    return int(bits, 2)

default = '.'
for _ in range(2):
    new_grid = grow_grid(grid)
    n = len(new_grid)
    for j in range(n):
        for i in range(n):
            index = pos2index(grid, i-2, j-2, default)
            new_grid[j][i] = algo[index]
    grid = new_grid
    default = algo[0b111111111 if default == '#' else 0b000000000]
    # for row in grid:
        # print(''.join(row))

total = 0
for row in grid:
    total += row.count('#')
print(total)
