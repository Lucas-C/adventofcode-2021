#!/usr/bin/env python3

OPENING_CHARS_OPPOSED = {'(': ')', '[': ']', '{': '}', '<': '>'}
CLOSING_CHARS_SCORES = {')': 3, ']': 57, '}': 1197, '>': 25137}

with open('input_10.txt') as in_file:
    chunks = [line.strip() for line in in_file]

score = 0
for chunk in chunks:
    stack = []
    for c in chunk:
        if c in OPENING_CHARS_OPPOSED:
            stack.append(c)
        else:
            assert c in CLOSING_CHARS_SCORES
            if c == OPENING_CHARS_OPPOSED[stack[-1]]:
                stack.pop()
            else:
                score += CLOSING_CHARS_SCORES[c]
                break
print(score)
    