#!/usr/bin/env python3

vents = []
max_x, max_y = 0, 0
with open('input_5.txt') as in_file:
    while True:
        line = in_file.readline()
        if not line:
            break
        x1y1, x2y2 = line.split(' -> ')
        x1, y1 = map(int, x1y1.split(','))
        if x1 > max_x: max_x = x1
        if y1 > max_y: max_y = y1
        x2, y2 = map(int, x2y2.split(','))
        if x2 > max_x: max_x = x2
        if y2 > max_y: max_y = y2
        vents.append(((x1, y1), (x2, y2)))
diagram = []
for _ in range(max_y+1):
    diagram.append([0]*(max_x+1))
for (x1, y1), (x2, y2) in vents:
    if x1 == x2:
        if y1 > y2: y1, y2 = y2, y1
        for y in range(y1, y2+1):
            diagram[y][x1] += 1
    elif y1 == y2:
        if x1 > x2: x1, x2 = x2, x1
        for x in range(x1, x2+1):
            diagram[y1][x] += 1
print(sum(sum(1 for n in line if n > 1) for line in diagram))
