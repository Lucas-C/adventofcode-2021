#!/usr/bin/env python3

with open('input_15.txt') as in_file:
    grid = [list(map(int, line.strip())) for line in in_file]

grid2 = []
for n_j in range(5):
    for j in range(len(grid)):
        row = []
        for n_i in range(5):
            row.extend([((risk+n_i+n_j-1)%9+1) for risk in grid[j]])
        grid2.append(row)
grid = grid2

j_max = len(grid) - 1
i_max = len(grid[0]) - 1

risk_map = {  # (x, y) -> total_risk
    (0, 0): 0
}
queue = [(0, 0)]

while queue:
    x, y = queue.pop(0)
    total_risk = risk_map[(x, y)]
    # print(f'Considering {x, y} with risk {total_risk} (#risk_map={len(risk_map)})')
    next_pos = []
    def is_pos_ok(i, j): return 0 <= i <= i_max and 0 <= j <= j_max
    if is_pos_ok(x+1, y): next_pos.append((x+1, y))
    if is_pos_ok(x-1, y): next_pos.append((x-1, y))
    if is_pos_ok(x, y+1): next_pos.append((x, y+1))
    if is_pos_ok(x, y-1): next_pos.append((x, y-1))
    for i, j in next_pos:
        risk = risk_map.get((i, j))
        new_risk = total_risk + grid[i][j]
        if risk is None or new_risk < risk:
            risk_map[i, j] = new_risk
            queue.append((i, j))

print(risk_map[i_max, j_max])  # takes ~30s, could be optimized
