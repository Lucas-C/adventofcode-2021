#!/usr/bin/env python3

with open('input_1.txt') as in_file:
    measurements = [int(line.strip()) for line in in_file]
largers = 0
for i, measurement in enumerate(measurements):
    if i and measurement > measurements[i-1]:
        largers += 1
print(largers)
