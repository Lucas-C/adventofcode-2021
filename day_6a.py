#!/usr/bin/env python3

with open('input_6.txt') as in_file:
    fishes = list(map(int, in_file.readline().strip().split(',')))
for _ in range(80):
    new_fishes = []
    for fish in fishes:
        if fish == 0:
            new_fishes.append(6)
            new_fishes.append(8)
        else:
            new_fishes.append(fish-1)
    fishes = new_fishes
print(len(fishes))