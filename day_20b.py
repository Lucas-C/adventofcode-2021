#!/usr/bin/env python3
# INSTALL: pip install bitarray
from bitarray import bitarray


with open('input_20.txt') as in_file:
    algo = bitarray(in_file.readline().strip().replace('#', '1').replace('.', '0'))
    in_file.readline()
    grid_ = [line.strip() for line in in_file]

n = len(grid_)
count = 50
N = n + 4*count  # final grid size

grid = bitarray(N*N)
grid.setall(0)
start = 2*count  # initial grid offset
for j, row in enumerate(grid_):
    for i, cell in enumerate(row):
        grid[(start+j)*N+start+i] = 1 if cell == '#' else 0

def pos2index(grid, start, end, i, j, default):
    index = 0
    for x, y in ((i-1, j-1), (i, j-1), (i+1, j-1),
                 (i-1, j),   (i, j),   (i+1, j),
                 (i-1, j+1), (i, j+1), (i+1, j+1)):
        if x < start or y < start or x >= end or y >= end:
            cell = default
        else:
            cell = grid[y*N+x]
        index <<= 1
        if cell:
            index +=1
    return index

default = 0
for _ in range(count):
    new_grid = grid.copy()
    start -= 2
    n += 4
    end = start + n
    for j in range(start, end):
        for i in range(start, end):
            index = pos2index(grid, start+2, end-2, i, j, default)
            new_grid[j*N+i] = algo[index]
    grid = new_grid
    default = algo[0b111111111 if default else 0b000000000]
    # for j in range(N):
        # print(''.join('#' if grid[j*N+i] else '.' for i in range(N)))

print(grid.count(1))
