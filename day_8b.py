#!/usr/bin/env python3

with open('input_8.txt') as in_file:
    lines = in_file.readlines()

total = 0
for line in lines:
    line = line.strip().split(' ')
    spd = [''] * 10  # Segments Per Digit
    spd[8] = 'abcdefg'
    while not all(segments for segments in spd):
        for pattern in line[:10]:
            s = ''.join(sorted(pattern))
            if len(pattern) == 2 and not spd[1]:
                spd[1] = s
            elif len(pattern) == 3 and not spd[7]:
                spd[7] = s
            elif len(pattern) == 4 and not spd[4]:
                spd[4] = s
            elif len(pattern) == 5:
                if not spd[3]:
                    if spd[1] and all(s in pattern for s in spd[1]):
                        spd[3] = s
                elif s != spd[3]:  # spd[3] is known
                    if not spd[5]:
                        if spd[6] and all(s in spd[6] for s in pattern):
                            spd[5] = s
                    elif s != spd[5] and not spd[2]:  # spd[3] & spd[5] are known
                        spd[2] = s
            elif len(pattern) == 6:
                if not spd[9]:
                    if spd[4] and all(s in pattern for s in spd[4]):
                        spd[9] = s
                elif s != spd[9]:  # spd[9] is known
                    if not spd[0]:
                        if spd[1] and all(s in pattern for s in spd[1]):
                            spd[0] = s
                    elif s != spd[6] and not spd[6]:  # spd[9] & spd[0] are known
                        spd[6] = s
    output = 0
    for pattern in line[11:]:
        output *= 10
        pattern = ''.join(sorted(pattern))
        value = next(d for d, p in enumerate(spd) if p == pattern)
        output += value
    total += output
print(total)
