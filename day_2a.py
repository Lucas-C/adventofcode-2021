#!/usr/bin/env python3

with open('input_2.txt') as in_file:
    moves = [line.strip() for line in in_file]
x, depth = 0, 0
for move in moves:
    delta = int(move.split(' ')[1])
    if move.startswith('down'):
        depth += delta
    elif move.startswith('up'):
        depth -= delta
    elif move.startswith('forward'):
        x += delta
print(x*depth)
