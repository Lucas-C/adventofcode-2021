#!/usr/bin/env python3

with open('input_4.txt') as in_file:
    numbers = list(map(int, in_file.readline().strip().split(',')))
    boards = []
    while True:
        if not in_file.readline():  # empty line or EOF
            break
        board = []
        for _ in range(5):
            board.extend(int(n.strip()) for n in in_file.readline().strip().split(' ') if n)
        boards.append(board)

for number in numbers:
    offset = 0
    for board_i, board in enumerate(list(boards)):
        for i in range(5):
            for j in range(5):
                if board[i*5+j] == number:
                    board[i*5+j] = None
                    if all(board[x*5+j] is None for x in range(5)) or all(board[i*5+y] is None for y in range(5)):
                        if len(boards) == 1:
                            print(number*sum(n for n in board if n is not None))
                            exit()
                        else:
                            boards = boards[:board_i-offset] + boards[board_i-offset+1:]
                            offset += 1
