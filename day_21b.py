#!/usr/bin/env python3

# ANALYSIS:
# * in the universe where players have the worst luck possible,
#   they only roll 1s, and it takes 7 turns for one to reach 21.
# * rolling 3d3 is equivalent to picking a single value among:
#   (3, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 8, 8, 8, 9)
# * this can be converted to a dict of occurences:
D3 = {
    3: 1,
    4: 3,
    5: 6,
    6: 7,
    7: 6,
    8: 3,
    9: 1,
}

def play(pos1, pos2, score1=0, score2=0):
    # print(f"play({pos1=}, {pos2=}, {score1=}, {score2=})")
    p1wins, p2wins = 0, 0
    for d3, times1 in D3.items():
        new_pos1 = pos1 + d3
        new_pos1 = (new_pos1 - 1)%10 + 1
        new_score1 = score1 + new_pos1
        if new_score1 >= 21:
            p1wins += times1
        else:
            for d3, times2 in D3.items():
                new_pos2 = pos2 + d3
                new_pos2 = (new_pos2 - 1)%10 + 1
                new_score2 = score2 + new_pos2
                if new_score2 >= 21:
                    p2wins += times1*times2
                else:
                    p1w, p2w = play(new_pos1, new_pos2, new_score1, new_score2)
                    p1wins += p1w*times1*times2
                    p2wins += p2w*times1*times2
    return p1wins, p2wins

def test_play():
    # P1 always wins as their next score is in [4, 10]:
    assert play(pos1=1, pos2=1, score1=17, score2=17) == (27, 0)
    # P2 always wins as their next score is in [4, 10] and P1 cannot win:
    assert play(pos1=1, pos2=1, score1=10, score2=17) == (0, 27*27)
    # P1 always wins except in 1 initial universe:
    assert play(pos1=1, pos2=1, score1=16, score2=17) == (26, 27)


if __name__ == '__main__':
    with open('input_21.txt') as in_file:
        pos1 = int(in_file.readline().strip()[-1])
        pos2 = int(in_file.readline().strip()[-1])
    # pos1, pos2 = 4, 8  # example provided
    print(max(play(pos1, pos2)))
