#!/usr/bin/env python3
import statistics
with open('input_7.txt') as in_file:
    crabs = list(map(int, in_file.readline().strip().split(',')))
median = int(statistics.median(crabs))
fuel = 0
for crab in crabs:
    fuel += abs(crab - median)
print(fuel)
