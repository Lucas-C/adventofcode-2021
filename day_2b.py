#!/usr/bin/env python3

with open('input_2.txt') as in_file:
    moves = [line.strip() for line in in_file]
x, depth, aim = 0, 0, 0
for move in moves:
    delta = int(move.split(' ')[1])
    if move.startswith('down'):
        aim += delta
    elif move.startswith('up'):
        aim -= delta
    elif move.startswith('forward'):
        x += delta
        depth += aim*delta
print(x*depth)
