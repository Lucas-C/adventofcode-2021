#!/usr/bin/env python3
import statistics
with open('input_7.txt') as in_file:
    crabs = list(map(int, in_file.readline().strip().split(',')))
min_fuel = None
for n in range(min(crabs), max(crabs)+1):
    fuel = 0
    for crab in crabs:
        diff = abs(crab - n)
        fuel += diff*(diff+1)//2
    if min_fuel is None or fuel < min_fuel:
        min_fuel = fuel
print(min_fuel)
