#!/usr/bin/env python3
from collections import defaultdict

rules = {}
with open('input_14.txt') as in_file:
    polymer = in_file.readline().strip()
    in_file.readline()  # empty line
    for line in in_file:
        line = line.strip()
        from_, to_ = line.split(' -> ')
        rules[from_] = to_

for step in range(10):
    next_gen = polymer[0]
    for c1, c2 in zip(polymer, polymer[1:]):
        if c1+c2 in rules:
            next_gen += rules[c1+c2]
        next_gen += c2
    polymer = next_gen

stats = defaultdict(int)
for c in polymer:
    stats[c] += 1

print(max(stats.values()) - min(stats.values()))
