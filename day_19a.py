#!/usr/bin/env python3
from collections import defaultdict


CS_CONVERTERS = [
    # 3 orientations in quadrant:
    lambda x, y, z: (+x, +y, +z),
    lambda x, y, z: (+z, +x, +y),
    lambda x, y, z: (+y, +z, +x),
    # 3 orientations in quadrant:
    lambda x, y, z: (-x, +z, +y),
    lambda x, y, z: (+y, -x, +z),
    lambda x, y, z: (+z, +y, -x),
    # 3 orientations in quadrant:
    lambda x, y, z: (+z, -y, +x),
    lambda x, y, z: (+x, +z, -y),
    lambda x, y, z: (-y, +x, +z),
    # 3 orientations in quadrant:
    lambda x, y, z: (+y, +x, -z),
    lambda x, y, z: (-z, +y, +x),
    lambda x, y, z: (+x, -z, +y),
    # 3 orientations in quadrant:
    lambda x, y, z: (-x, -z, -y),
    lambda x, y, z: (-y, -x, -z),
    lambda x, y, z: (-z, -y, -x),
    # 3 orientations in quadrant:
    lambda x, y, z: (+x, -y, -z),
    lambda x, y, z: (-z, +x, -y),
    lambda x, y, z: (-y, -z, +x),
    # 3 orientations in quadrant:
    lambda x, y, z: (-y, +z, -x),
    lambda x, y, z: (-x, -y, +z),
    lambda x, y, z: (+z, -x, -y),
    # 3 orientations in quadrant:
    lambda x, y, z: (-z, -x, +y),
    lambda x, y, z: (+y, -z, -x),
    lambda x, y, z: (-x, +y, -z),
]
assert len(set(csc(1, 2, 3) for csc in CS_CONVERTERS)) == 24  # Ensure no duplicates


def read_scanner(lines):
    assert lines.pop(0).startswith('--- scanner ')
    beacons = []
    while lines and lines[0]:
        pos = lines.pop(0).split(',')
        beacons.append((int(pos[0]), int(pos[1]), int(pos[2])))
    if lines:
        lines.pop(0)
    return beacons


with open('input_19.txt') as in_file:
    lines = [line.strip() for line in in_file]

scanners = []
while lines:
    scanners.append(read_scanner(lines))

# "Brute force" approach, very long execution time :(
uniq_beacons = set(scanners.pop(0))
si = 0  # index in "scanners"
while scanners:
    stop = False
    # OPTIM: maybe beacons most commonly found in scanners should be tested first?
    for i, b0 in enumerate(uniq_beacons):
        for j, cs_converter in enumerate(CS_CONVERTERS):
            beacons_oriented = [cs_converter(*pos) for pos in scanners[si]]
            for k, b1 in enumerate(beacons_oriented):
                shift = (b0[0]-b1[0], b0[1]-b1[1], b0[2]-b1[2])
                beacons_shifted = [(pos[0]+shift[0], pos[1]+shift[1], pos[2]+shift[2]) for pos in beacons_oriented]
                matching_count = sum(beacons_shifted.count(pos) for pos in uniq_beacons)
                if matching_count >= 12:
                    print(f"BINGO: {si=} {i=} CS={j} {k=} {matching_count=} {shift=} #scanners={len(scanners)} #uniq_beacons={len(uniq_beacons)}")
                    uniq_beacons.update(beacons_shifted)
                    stop = True
                    del scanners[si]
                    si = 0
                    break
            if stop: break
        if stop: break
    if not stop: si += 1
    assert si < len(scanners)  # Currently fails on last scanner after 8min:
# BINGO: si=5 i=0 CS=21 k=17 matching_count=12 shift=(1088, -33, 128) #scanners=24 #uniq_beacons=26
# BINGO: si=1 i=6 CS=6 k=23 matching_count=12 shift=(2312, 90, 157) #scanners=23 #uniq_beacons=40
# BINGO: si=1 i=1 CS=15 k=15 matching_count=12 shift=(1074, -28, 1220) #scanners=22 #uniq_beacons=54
# BINGO: si=2 i=0 CS=13 k=5 matching_count=12 shift=(2385, 1329, 124) #scanners=21 #uniq_beacons=68
# BINGO: si=2 i=0 CS=2 k=13 matching_count=18 shift=(-84, 85, 1346) #scanners=20 #uniq_beacons=82
# BINGO: si=4 i=4 CS=18 k=12 matching_count=12 shift=(-64, 22, 2441) #scanners=19 #uniq_beacons=90
# BINGO: si=5 i=4 CS=23 k=23 matching_count=12 shift=(-57, 1244, 1186) #scanners=18 #uniq_beacons=104
# BINGO: si=3 i=7 CS=20 k=2 matching_count=12 shift=(1127, 1288, 2468) #scanners=17 #uniq_beacons=118
# BINGO: si=5 i=4 CS=8 k=4 matching_count=21 shift=(-14, 1322, 2386) #scanners=16 #uniq_beacons=131
# BINGO: si=3 i=7 CS=14 k=0 matching_count=12 shift=(18, 1160, 3720) #scanners=15 #uniq_beacons=136
# BINGO: si=5 i=0 CS=10 k=8 matching_count=12 shift=(-85, -1109, -30) #scanners=14 #uniq_beacons=149
# BINGO: si=5 i=8 CS=9 k=17 matching_count=18 shift=(1095, 1250, 3619) #scanners=13 #uniq_beacons=162
# BINGO: si=6 i=2 CS=1 k=2 matching_count=12 shift=(1226, 1271, 4896) #scanners=12 #uniq_beacons=170
# BINGO: si=0 i=3 CS=12 k=10 matching_count=12 shift=(1246, 60, 4891) #scanners=11 #uniq_beacons=183
# BINGO: si=4 i=1 CS=3 k=18 matching_count=12 shift=(1204, 1351, 6127) #scanners=10 #uniq_beacons=197
# BINGO: si=0 i=2 CS=5 k=22 matching_count=12 shift=(1066, 1292, 7276) #scanners=9 #uniq_beacons=210
# BINGO: si=0 i=49 CS=16 k=22 matching_count=12 shift=(0, 1283, 6096) #scanners=8 #uniq_beacons=224
# BINGO: si=0 i=48 CS=7 k=18 matching_count=12 shift=(1165, 2494, 7241) #scanners=7 #uniq_beacons=238
# BINGO: si=0 i=26 CS=22 k=13 matching_count=12 shift=(-1270, 1352, 4893) #scanners=6 #uniq_beacons=252
# BINGO: si=0 i=12 CS=17 k=1 matching_count=12 shift=(-1177, 1290, 1200) #scanners=5 #uniq_beacons=265
# BINGO: si=0 i=1 CS=11 k=3 matching_count=12 shift=(1089, 2452, 4853) #scanners=4 #uniq_beacons=279
# BINGO: si=0 i=3 CS=19 k=8 matching_count=18 shift=(1244, 19, 6152) #scanners=3 #uniq_beacons=292
# BINGO: si=0 i=21 CS=4 k=18 matching_count=18 shift=(-1338, 1189, 3601) #scanners=2 #uniq_beacons=299
# BINGO: si=0 i=28 CS=6 k=21 matching_count=24 shift=(25, 1313, 4787) #scanners=1 #uniq_beacons=307

print(len(uniq_beacons))
